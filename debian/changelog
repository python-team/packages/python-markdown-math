python-markdown-math (0.8-2) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dh-python.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 15 Oct 2022 02:48:48 -0000

python-markdown-math (0.8-1) unstable; urgency=medium

  * New upstream release.
  * Drop markdown_3.3.diff, included in the new release.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 07 Nov 2020 13:42:17 +0300

python-markdown-math (0.7-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Dmitry Shachnev ]
  * Backport upstream patch to make the tests pass with Python-Markdown 3.3.
    - Bump the build-dependency and test dependency on python3-markdown.
  * Update Uploader field with new Debian Python Team contact address.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 09 Oct 2020 13:14:40 +0300

python-markdown-math (0.7-1) unstable; urgency=medium

  * New upstream release.
  * Update my OpenPGP key in debian/upstream/signing-key.asc.
  * Update to debhelper compat level 13.
  * Set Rules-Requires-Root: no.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 12 Jun 2020 11:22:48 +0300

python-markdown-math (0.6-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Dmitry Shachnev ]
  * Make the autopkgtest depend on python3-all, and use py3versions -s
    instead of -i.
  * Update to debhelper compat level 12.
  * Bump Standards-Version to 4.5.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 20 Mar 2020 16:17:13 +0300

python-markdown-math (0.6-1) unstable; urgency=medium

  * Initial release (closes: #901166).

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 15 Jun 2018 16:51:37 +0300
